
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BackandService } from '@backand/angular2-sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';

}