import { Dare2codePage } from './app.po';

describe('dare2code App', function() {
  let page: Dare2codePage;

  beforeEach(() => {
    page = new Dare2codePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
